#!/bin/bash

SCRIPT_DIR="$(dirname "$0")"
source "$SCRIPT_DIR/colors.sh"

BATTERY_INFO="$(upower -i /org/freedesktop/UPower/devices/battery_BAT0 | sed -E 's/\s+/ /g')"
PERCENT="$(echo "$BATTERY_INFO" | grep -oP '(?<=percentage: )\d+(?=%)')"
STATE="$(echo "$BATTERY_INFO" | grep -oP '(?<=state: )[0-9a-zA-Z-]+%')"

if [ -n "$(echo "$STATE" | grep -o 'discharg')" ]; then
  STATE=''
else
  STATE='+'
fi
echo "BATTERY: [ $PERCENT%$STATE ]"
echo "BAT:$PERCENT%$STATE"
if [ "$PERCENT" -lt '25' ]; then
  echo "$RED_24"
elif [ "$PERCENT" -lt '50' ]; then
  echo "$ORANGE_24"
elif [ "$PERCENT" -lt '75' ]; then
  echo "$YELLOW_24"
fi

exit 0

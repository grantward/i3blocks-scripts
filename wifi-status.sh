#!/bin/bash

SCRIPT_DIR="$(dirname "$0")"
source "$SCRIPT_DIR/colors.sh"

STATUS="$(sudo -n wpa_cli status)"

SSID="$(grep -oP '(?<=^ssid=)[a-zA-Z0-9-_ ]+' <<< "$STATUS")"
IP="$(grep -oP '(?<=ip_address=)\d+\.\d+\.\d+\.\d+' <<< "$STATUS")"

if [ -n "$SSID" ]; then
  echo "$SSID:$IP"
else
  echo 'WIFI: NOT CONNECTED'
  echo 'WIFI:OFF'
  echo "$RED_24"
fi

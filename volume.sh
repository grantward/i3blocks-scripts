#!/bin/bash

SCRIPT_DIR="$(dirname "$0")"
source "$SCRIPT_DIR/colors.sh"

VOL_LINE="$(amixer | grep 'Front Left: Playback')"
VOL="$(grep -oP '\d+%' <<< "$VOL_LINE")"
STATUS="$(grep -oP '(?<=\[)on|off(?=\])' <<< "$VOL_LINE")"


echo "VOLUME: $VOL"
echo "VOL:$VOL"
if [ "$STATUS" = 'off' ]; then
  echo "$RED_24"
fi

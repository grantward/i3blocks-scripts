#!/bin/bash

CLEAR='\033[0m'

RED='\033[0;31m'
GREEN='\033[0;32m'
YELLOW='\033[0;33m'

RED_24='#FF0000'
GREEN_24='#00FF00'
ORANGE_24='#FF8000'
YELLOW_24='#FFFF00'

echo_color () {
  echo -e "${1}${2}${CLEAR}"
}

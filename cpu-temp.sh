#!/bin/bash

SCRIPTS_DIR="$(dirname "$0")"
source "$SCRIPTS_DIR/colors.sh"

TEMP_PAT='[-+]\d+\.\d.[CF]'

get_number () {
  grep -oP '[-]?\d+(?=\.\d)' <<< "$1"
}

is_higher_temp () {
  (( "$(get_number "$1")" >= "$(get_number "$2")" ))
}

TEMP_LINE="$(sensors | grep 'Package id 0:')"
TEMP="$(grep -oP "(?<=:  )$TEMP_PAT" <<< "$TEMP_LINE")"
HIGH="$(grep -oP "(?<=high = )$TEMP_PAT" <<< "$TEMP_LINE")"
CRIT="$(grep -oP "(?<= crit = )$TEMP_PAT" <<< "$TEMP_LINE")"


echo "CPU: $TEMP"
echo "CPU:$TEMP"
if is_higher_temp "$TEMP" "$CRIT"; then
  PRINT_COLOR="$RED_24"
elif is_higher_temp "$TEMP" "$HIGH"; then
  PRINT_COLOR="$ORANGE_24"
fi
